#ifndef triangle_h
#define triangle_h

#include "Point.hpp"
#include <iostream>
#include <string>
#include <math.h>
#include <vector>

class Triangle{
    private:
        Point point1,point2,point3;
    public:
        Triangle(const Point point1,const Point point2,const Point point3);
        inline Point getPoint1 () const;
        inline Point getPoint2 () const;
        inline Point getPoint3 () const;
        void setPoint1(const float x, const float y);
        void setPoint2(const float x, const float y);
        void setPoint3(const float x, const float y);
        inline float getDistance(Point p1,Point p2);
        inline float Base();
        inline float Hauteur();
        inline float Surface();
        inline std::vector<float> Longueurs();
        inline bool Isocele();
        inline bool Rectangle();
        inline bool Equilateral();
        void Afficher();
};

#endif