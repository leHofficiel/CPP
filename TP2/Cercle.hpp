#ifndef cercle_h
#define cercle_h

#include "Point.hpp"
#include <iostream>
#include <string>
#include <math.h>

class Cercle{
    private:
        Point pointCentre;
        int diametre;
    public:
        Cercle(const Point centre, const int diametre);
        inline Point getCentre() const;
        inline int getDiametre() const;
        void setCentre(const float x, const float y);
        void setDiametre(const int diam);
        inline float getPerimetre() const;
        inline float getSurface() const;
        bool estSurCercle(Point& point);
        bool estDansCercle(Point& point);
        void Afficher();
};
#endif