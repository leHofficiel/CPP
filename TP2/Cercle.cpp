#include "Cercle.hpp"

/// @brief Constructeur permettant la création d'un objet de type Cercle
/// @param centre : position du centre du cercle
/// @param diametre : diamètre que l'on souhaite attribuer au cercle
Cercle::Cercle(const Point centre, const int diametre)
{
    this->pointCentre=centre;
    this->diametre=diametre;
}

/// @brief Fonction renvoyant le point correspondant au centre du cercle
/// @return retourne les coordonnées du centre du cercle (struct Point)
Point Cercle::getCentre() const
{
    return this-> pointCentre;
}

/// @brief Fonction renvoyant le diametre d'un cercle
/// @return retourne le diametre de type int
int Cercle::getDiametre() const{
    return this->diametre;
}

/// @brief Fonction renvoyant le périmètre d'un cercle
/// @return retourne le périmètre de type int
float Cercle::getPerimetre() const {
    return 2*M_PI*(diametre/2);
}

/// @brief Fonction renvoyant la surface d'un cercle
/// @return retourne la surface de type int
float Cercle::getSurface() const
{
    return M_PI*(diametre/2)*(diametre/2);
}

/// @brief Méthode permettant de définir le centre d'un cercle
/// @param x : coordonnée en abscisse du centre (float)
/// @param y : coordonnée en ordonnée du centre (float)
void Cercle::setCentre(float x, float y)
{
    this->pointCentre.x=x;
    this->pointCentre.y=y;
}

/// @brief Méthode permettant de définir le diamètre d'un cercle
/// @param diam : diamètre de type int
void Cercle::setDiametre(int diam)
{
    this->diametre=diam;
}

/// @brief Fonction permettant de vérifié si un point est situé sur le cercle
/// @param point : Point sur lequelle la vérification est faites
/// @return retourne true (1) si le point est sur le cercle, sinon false (0)
bool Cercle::estSurCercle(Point& point)
{
    return sqrt(pow((point.x-pointCentre.x),2) + pow((point.y-pointCentre.y),2))== diametre/2;   
}

/// @brief Fonction permettant de vérifié si un point est situé DANS le cercle (et non sur)
/// @param point : Point sur lequelle la vérification est faites
/// @return retourne true (1) si le point est DANS le cercle, sinon false (0)
bool Cercle::estDansCercle(Point& point)
{
    return sqrt(pow((point.x-pointCentre.x),2) + pow((point.y-pointCentre.y),2))<diametre/2;   
}

/// @brief Méthode permettant l'affichage de toute les informations concernant un cercle
void Cercle::Afficher()
{
    Point p2;
    p2.x=11.43;
    p2.y=7.22;
    std::cout<<"________________________________________________________________"<<std::endl;
    std::cout<< "Cercle\nCoordonnées du centre : (" << getCentre().x << "," << getCentre().y << ")"<< std::endl;
    std::cout<< "Diamètre : " << getDiametre() <<std::endl;
    std::cout<< "Périmètre : " << getPerimetre() << ", Surface : "<< getSurface() << std::endl;
    std::cout<< "Est sur cercle ? " << estSurCercle(p2) << std::endl;
    std::cout<< "Est dans cercle ? " << estDansCercle(p2)<< std::endl;
}