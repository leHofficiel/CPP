#include "Rectangle.hpp"

/// @brief Constructeur permettant la création d'un objet de type Rectangle
/// @param longueur : longueur que l'on souhaite donner au rectangle, de type int
/// @param larg : largeur que l'on souhaite donner au rectangle, de type int
/// @param coin : coordonnées que l'on souhaite attribué au coin gauche du rectangle, type struct Point
Rectangle::Rectangle (const int longueur, const int larg, const Point coin)
{
    this->longueur=longueur;
    this->largeur=larg;
    this->coin=coin;
}

/// @brief Fonction renvoyant la longueur d'un rectangle
/// @return retourne la longueur de type int
int Rectangle::getLongueur() const
{
    return this->longueur;
}

/// @brief Fonction renvoyant la largeur d'un rectangle
/// @return retourne la largeur de type int
int Rectangle::getLargeur() const 
{
    return this->largeur;
}

/// @brief Fonction qui renvoie le coin gauche d'un rectangle
/// @return retourne le coin, type struct Point
Point Rectangle::getCoin() const{
    return this->coin;
}

/// @brief Fonction qui calcule le périmètre d'un rectangle
/// @return retourne le périmètre de type int
int Rectangle::getPerimetre() const
{
    return (longueur*2)+(largeur*2);
}

/// @brief Fonction qui calcule la surface d'un rectangle
/// @return retourne la surface de type int
int Rectangle::getSurface() const
{
    return longueur*largeur;
}

/// @brief Fonction qui compare le perimètre d'un rectangle avec un autre
/// @param rectangle: le rectangle avec lequel on veut faire la comparaison
/// @return retourne true (1) s'il est plus grand, sinon false (0)
bool Rectangle::perimetrePlusGrand(Rectangle& rectangle)
{
    return this->getPerimetre() > rectangle.getPerimetre();
}

/// @brief Fonction qui compare la surface d'un rectangle avec un autre
/// @param rectangle: le rectangle avec lequel on veut faire la comparaison
/// @return retourne true (1) si la surface est plus grande, sinon false (0)
bool Rectangle::surfacePlusGrande(Rectangle& rectangle)
{
    return this->getSurface() > rectangle.getSurface();
}

/// @brief Méthode permettant de définir la longueur d'un rectangle
/// @param longue : longueur que l'on souhaite attribuer au rectangle de type int
void Rectangle::setLongueur (const int longue)
{
    this->longueur=longue;
}

/// @brief Méthode permettant de définir la largeur d'un rectangle
/// @param larg : largeur que l'on souhaite attribuer au rectangle de type int
void Rectangle::setLargeur (const int larg)
{
    this->largeur=larg;
}

/// @brief Méthode permettant de définir les coordonnées du coin gauche du rectangle
/// @param x : coordonnée en abscisse du coin, de type float
/// @param y : coordonnée en ordonnée du coin, de type float
void Rectangle::setCoin(const float x, const float y)
{
    this->coin.x= x;
    this->coin.y= y;

}

/// @brief Méthode permettant l'affichage de toute les informations concernant un rectangle
void Rectangle::Afficher()
{
    Rectangle compare(40,23,coin);
    std::cout<< "Rectangle :\nLongueur : " << getLongueur() << ", Largeur : "<< getLargeur() << std::endl;
    std::cout<< "Coordonnées du coin gauche : (" << getCoin().x << "," << getCoin().y << ")"<< std::endl;
    std::cout<< "Périmètre : " << getPerimetre() << ", Surface : "<< getSurface() << std::endl;
    std::cout<< "Est plus grand périmètre ? " << perimetrePlusGrand(compare) << std::endl;
    std::cout<< "Est plus grande surface ? " << surfacePlusGrande(compare) << std::endl;
}