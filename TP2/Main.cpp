#include "Cercle.hpp"
#include "Point.hpp"
#include "Rectangle.hpp"
#include "Triangle.hpp"
#include <iostream>
#include <string>

int main()
{
    Point coinRectangle;
    coinRectangle.x=2.82;
    coinRectangle.y=3.39;
    Rectangle rec(20,10,coinRectangle);
    rec.Afficher();

    Point centreCercle;
    centreCercle.x=12.43;
    centreCercle.y=7.22;
    Cercle cercle(centreCercle,4);
    cercle.Afficher();

    Point p1,p2,p3;
    p1.x=1;
    p1.y=4.23;
    p2.x=1.45;
    p2.y=3;
    p3.x=4;
    p3.y=1;
    Triangle triangle(p1,p2,p3);
    triangle.Afficher();    
}   