#ifndef rectangle_h
#define rectangle_h

#include "Point.hpp"
#include <iostream>
#include <string>

class Rectangle{
    private:
        int longueur;
        int largeur;
        Point coin;
    public:
        Rectangle(const int longue, const int large, const Point coin);
        inline int getLongueur() const;
        inline int getLargeur() const;
        inline Point getCoin() const;
        void setLongueur(const int longueur);
        void setLargeur(const int largeur);
        void setCoin(const float x, const float y);
        inline int getPerimetre() const;
        inline int getSurface() const;
        bool perimetrePlusGrand(Rectangle& rectangle);
        bool surfacePlusGrande(Rectangle& rectangle);
        void Afficher();
        
};

#endif