#include "Triangle.hpp"

/// @brief Constructeur permettant la création d'un objet de type Triangle
/// @param p1 : Premier point du triangle
/// @param p2 : Deuxième point du triangle
/// @param p3 : Troisième point du triangle 
Triangle::Triangle(const Point p1, const Point p2, const Point p3)
{
    this->point1=p1;
    this->point2=p2;
    this->point3=p3;
}

/// @brief Fonction renvoyant le 1er point du triangle
/// @return retourne le point (struct Point)
Point Triangle::getPoint1() const 
{
    return this->point1;
}

/// @brief Fonction renvoyant le 2eme point du triangle
/// @return retourne le point (struct Point)
Point Triangle::getPoint2() const 
{
    return this->point2;
}

/// @brief Fonction renvoyant le 3eme point du triangle
/// @return retourne le point (struct Point)
Point Triangle::getPoint3() const 
{
    return this->point3;
}

/// @brief Méthode permettant de définir le 1er point d'un triangle
/// @param x : coordonnée en abscisse du point (float)
/// @param y : coordonnée en ordonnée du point (float)
void Triangle::setPoint1(const float x, const float y)
{
    this->point1.x=x;
    this->point1.y=y;
}

/// @brief Méthode permettant de définir 2eme point d'un triangle
/// @param x : coordonnée en abscisse du point (float)
/// @param y : coordonnée en ordonnée du point (float)
void Triangle::setPoint2(const float x, const float y)
{
    this->point2.x=x;
    this->point2.y=y;
}

/// @brief Méthode permettant de définir le 3eme point d'un triangle
/// @param x : coordonnée en abscisse du point (float)
/// @param y : coordonnée en ordonnée du point (float)
void Triangle::setPoint3(const float x, const float y)
{
    this->point3.x=x;
    this->point3.y=y;
}

/// @brief Fonction renvoyant la distance entre 2 points
/// @param p1 : 1er point
/// @param p2 : 2e point 
/// @return retourne la distance en float
float Triangle::getDistance(Point p1, Point p2)
{
    return sqrt(pow((p2.x-p1.x),2) + pow((p2.y-p1.y),2));
}

/// @brief Fonction renvoyant le coté le plus long d'un triangle
/// @return retourne la longueur du coté le plus long en float
float Triangle::Base()
{   
    float base=Longueurs()[0];
    for( const float & dist : Longueurs() ) {
        if (dist > base) base=dist;
    }
    return base;
}

/// @brief Fonction calculant la surface d'un triangle
/// @return retourne la surface en float
float Triangle::Surface()
{   
    float demiPerimetre=(Longueurs()[0]+Longueurs()[1]+Longueurs()[2])/2.0;
    return sqrtf(demiPerimetre*(demiPerimetre-Longueurs()[0])*(demiPerimetre-Longueurs()[1])*(demiPerimetre-Longueurs()[2]));
}

/// @brief Fonction renvoyant toutes les longueurs de chaques cotés d'un triangle
/// @return retourne un vecteur contenant chaque longueur en float
std::vector<float> Triangle::Longueurs()
{
    std::vector<float> longueurs={getDistance(point1,point2),getDistance(point1,point3),getDistance(point2,point3)};
    return longueurs;
}

/// @brief Fonction calculant la hauteur d'un triangle
/// @return retourne la hauteur en float
float Triangle::Hauteur()
{   
    return 2.0*Surface()/Base();
}

/// @brief Fonction vérifiant si un triangle est isocèle
/// @return retourne true (1) si le triangle est isocèle, sinon false(0)
bool Triangle::Isocele()
{
    return (Longueurs()[0]==Longueurs()[1] || Longueurs()[0]==Longueurs()[2] || Longueurs()[2]==Longueurs()[3] );
}

/// @brief Fonction vérifiant si un triangle est équilatéral
/// @return retourne true (1) si le triangle est équilatéral, sinon false(0)
bool Triangle::Equilateral()
{
    return (Longueurs()[0]==Longueurs()[1] && Longueurs()[0]== Longueurs()[3] );
}

/// @brief Fonction vérifiant si un triangle est rectangle
/// @return retourne true (1) si le triangle est rectangle, sinon false(0)
bool Triangle::Rectangle()
{

    bool res= false;
    if (powf(Longueurs()[0],2)==(powf(Longueurs()[1],2)+powf(Longueurs()[2],2)))
        res=true;
    else if (powf(Longueurs()[1],2)==(powf(Longueurs()[0],2)+powf(Longueurs()[2],2)))
        res =true;
    else if (powf(Longueurs()[2],2)==(powf(Longueurs()[0],2)+powf(Longueurs()[1],2)))
        res= true;
    
    return res;
    
}

/// @brief Méthode permettant l'affichage de toute les informations concernant un triangle
void Triangle::Afficher()
{
    std::cout<<"________________________________________________________________"<<std::endl;
    std::cout<< "Triangle\nCoordonnées du point A : (" << getPoint1().x << "," << getPoint1().y << ")"<< std::endl;
    std::cout<< "Coordonnées du point B : (" << getPoint2().x << "," << getPoint2().y << ")"<< std::endl;
    std::cout<< "Coordonnées du point C : (" << getPoint3().x << "," << getPoint3().y << ")"<< std::endl;
    std::cout<< "Base : " << Base() <<std::endl;
    std::cout<< "Hauteur : " << Hauteur() <<std::endl;
    std::cout<< "Longueurs :" << Longueurs()[0] << " , " << Longueurs()[1] << " , " << Longueurs()[2] <<std::endl;
    std::cout<< "Surface : " << Surface() <<std::endl;
    std::cout<< "Isocele ? " << Isocele() <<std::endl;
    std::cout<< "Equilateral ? " << Equilateral() <<std::endl;
    std::cout<< "Rectangle ? " << Rectangle() <<std::endl;
    
}