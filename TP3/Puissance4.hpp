#ifndef puissance_h
#define puissance_h


#include <iostream>
#include <string>
#include <vector>
#include "Player.hpp"
#include "Grille.hpp"

class Puissance4{
    private:
        Player player1;
        Player player2;
        Player winner;
        Grille grille;
    public:
        Puissance4();
        void Place(int y,Symbole p);
        void PlayRound(Player player);
        void InitPlayer();
        void StartGame();
        bool Gagne();
        bool GrilleFull();
};
#endif