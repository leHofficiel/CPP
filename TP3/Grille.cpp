#include "Grille.hpp"
#include "Case.hpp"

using namespace std;

Grille::Grille(){

}

Grille::Grille(int width, int length)
{
    this->grille.resize(width,std::vector<Case>(length));
    
}

void Grille::AfficherGrille()
{
    cout<< "---------Grille---------"<<endl;
    for (long unsigned int i = 0; i < grille.size(); i++)
    {
        cout<<"      ";
        for (long unsigned int j = 0; j < grille[i].size(); j++)
        {
            if(j!=0)
                cout<<"  |  ";
            if (grille[i][j].getSymbole()==1)
                cout << "X";
            else if (grille[i][j].getSymbole()==2)
                cout << "O";
            else if (grille[i][j].getSymbole()==0)
                cout << "-";
        }
        cout<<endl;
    }
}

void Grille::Remplir(int w, int l,Symbole symb)
{
    
    grille[w][l].setSymbole(symb);
}

std::vector<std::vector<Case>> Grille::getGrille()
{
    return this->grille;
}