#ifndef player_h
#define player_h

#include <iostream>
#include <string>
#include <vector>
#include"Case.hpp"

class Player
{
    private:
        std::string name;
        Symbole symbole;
    public:
        Player();
        std::string getName();
        void setName(std::string name);
        Symbole getSymbole();
        void setSymbole(Symbole symbole);
};

#endif
