#include "Puissance4.hpp"

using namespace std;

Puissance4::Puissance4(){

    this->grille=Grille(4,7);
}

void Puissance4::PlayRound(Player player){

    cout<<player.getName()<< " Ou veut tu placer ton pion ? " << endl;
    cout<< "Colonne :"<<endl;
    int colonne;
    cin>> colonne;
    Place(colonne,player.getSymbole());
    grille.AfficherGrille();
}

void Puissance4::InitPlayer()
{
    cout<<"Joueur 1 entre ton nom !"<<endl;
    string name1;
    cin>>name1;
    cout<<"Joueur 2 entre ton nom !"<<endl;
    string name2;
    cin>>name2;
    player1.setName(name1);
    player2.setName(name2);
    player1.setSymbole(X);
    player2.setSymbole(O);
    grille.AfficherGrille();

}

void Puissance4::StartGame(){
    
    while (GrilleFull()!=true) //&& Gagne()!=true)
    {
        PlayRound(player1);
        PlayRound(player2);
    }

    if (Gagne()==true)
    {
        cout<< "Félicitation " << winner.getName();
    }
    
    
}

void Puissance4::Place(int y,Symbole player){

    int lignedubas=3;
    cout<<lignedubas<<endl;
    while(grille.getGrille()[lignedubas][y].getSymbole()!=Empty)
    {
        lignedubas--;
    }
    cout<<lignedubas<<endl;
    grille.Remplir(lignedubas,y,player);
    
}

bool Puissance4::Gagne()
{
    bool res=false;

    // //Diagonales
    // for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    // {
    //     for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
    //     {
    // if(((grille.getGrille()[1][1].getSymbole())==(grille.getGrille()[2][2].getSymbole()) && (grille.getGrille()[2][2].getSymbole()) ==(grille.getGrille()[0][0].getSymbole()))&& grille.getGrille()[0][0].getSymbole()!=Empty)
    // {
    //     res=true;
    // }
    // if(((grille.getGrille()[0][2].getSymbole())==(grille.getGrille()[1][1].getSymbole()) && (grille.getGrille()[2][2].getSymbole()) ==(grille.getGrille()[2][0].getSymbole()) )&& grille.getGrille()[0][2].getSymbole()!=Empty)
    //     res=true;
         
    //     }
    // }
    // //LIGNES
    // for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    // {
        
    //     Symbole symb=grille.getGrille()[i][0].getSymbole();
    //     //cout<< "sym " << symb;
    //     int nbsymb=0;
    //     //cout<< "nbsymb " << nbsymb;
    //     for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
    //     {
    //         if (grille.getGrille()[i][j].getSymbole()==symb && symb!=Empty)
    //         {
    //             nbsymb++;
    //         }            
    //     }
    //     //cout<< "nbsymb " << nbsymb;
    //     if(nbsymb==3)
    //         res=true;
    //     if (symb==X)
    //         winner=player1;
    //     else
    //         winner=player2;        
    // }
    
    // //Colonnes
    // for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    // {
        
    //     Symbole symb=grille.getGrille()[0][i].getSymbole();
    //     //cout<< "sym " << symb;
    //     int nbsymb=0;
    //     //cout<< "nbsymb " << nbsymb;
    //     for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
    //     {
    //         if (grille.getGrille()[j][i].getSymbole()==symb && symb!=Empty)
    //         {
    //             nbsymb++;
    //         }            
    //     }
    //     //cout<< "nbsymb " << nbsymb;
    //     if(nbsymb==3)
    //         res=true;
    //     if (symb==X)
    //         winner=player1;
    //     else
    //         winner=player2;
    //}
    return res;
}

bool Puissance4::GrilleFull()
{
    bool res=true;
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        cout<<"      ";
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[i][j].getSymbole()==Empty)
            {
                res=false;
            }
            
        }
    }
    return res;
}
