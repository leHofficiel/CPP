#ifndef game_h
#define game_h

#include <iostream>
#include <string>
#include <vector>
#include "Player.hpp"
#include "Grille.hpp"

class Morpion{
    private:
        Player player1;
        Player player2;
        Player winner;
        Grille grille;
    public:
        Morpion();
        void Place(int x,int y,Symbole p);
        void PlayRound(Player player);
        void InitPlayer();
        void StartGame();
        bool Gagne();
        bool GrilleFull();
};

#endif

