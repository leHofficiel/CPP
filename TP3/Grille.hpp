#ifndef grille_h
#define grille_h

#include <iostream>
#include "Case.hpp"
#include <string>
#include <vector>

class Grille{
    private:
        std::vector<std::vector<Case>> grille;
    public:
        Grille();
        Grille(int width, int length);
        void AfficherGrille();
        std::vector<std::vector<Case>> getGrille();
        void Remplir(int w, int l,Symbole symb);
};
#endif