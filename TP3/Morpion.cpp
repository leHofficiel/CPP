#include "Morpion.hpp"

using namespace std;

Morpion::Morpion(){

    this->grille=Grille(3,3);
}

void Morpion::PlayRound(Player player){

    cout<<"\n" << player.getName()<< ", ou veut tu placer ton symbole " << endl;
    cout<< "Ligne :"<<endl;
    int ligne;
    cin>> ligne;
    cout<< "Colonne :"<<endl;
    int colonne;
    cin>> colonne;
    Place(ligne,colonne,player.getSymbole());
    grille.AfficherGrille();
}

void Morpion::InitPlayer()
{
    cout<<"Joueur 1 entre ton nom !"<<endl;
    string name1;
    cin>>name1;
    cout<<"Joueur 2 entre ton nom !"<<endl;
    string name2;
    cin>>name2;
    player1.setName(name1);
    player2.setName(name2);
    player1.setSymbole(X);
    player2.setSymbole(O);
    grille.AfficherGrille();

}

void Morpion::StartGame(){
    
    while (GrilleFull()!=true && Gagne()!=true)
    {
        PlayRound(player1);
        PlayRound(player2);
    }

    if (Gagne()==true)
    {
        cout<< "Félicitation " << winner.getName();
    }
    
    
}

void Morpion::Place(int x,int y,Symbole player){
    if(grille.getGrille()[x][y].isFree())
    {
        grille.Remplir(x,y,player);
    }
}

bool Morpion::Gagne()
{
    bool res=false;

    //Diagonales
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
    if(((grille.getGrille()[1][1].getSymbole())==(grille.getGrille()[2][2].getSymbole()) && (grille.getGrille()[2][2].getSymbole()) ==(grille.getGrille()[0][0].getSymbole()))&& grille.getGrille()[0][0].getSymbole()!=Empty)
    {
        res=true;
    }
    if(((grille.getGrille()[0][2].getSymbole())==(grille.getGrille()[1][1].getSymbole()) && (grille.getGrille()[2][2].getSymbole()) ==(grille.getGrille()[2][0].getSymbole()) )&& grille.getGrille()[0][2].getSymbole()!=Empty)
        res=true;
         
        }
    }
    //LIGNES
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        
        Symbole symb=grille.getGrille()[i][0].getSymbole();
        //cout<< "sym " << symb;
        int nbsymb=0;
        //cout<< "nbsymb " << nbsymb;
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[i][j].getSymbole()==symb && symb!=Empty)
            {
                nbsymb++;
            }            
        }
        //cout<< "nbsymb " << nbsymb;
        if(nbsymb==3)
            res=true;
        if (symb==X)
            winner=player1;
        else
            winner=player2;        
    }
    

    //Colonnes
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        
        Symbole symb=grille.getGrille()[0][i].getSymbole();
        //cout<< "sym " << symb;
        int nbsymb=0;
        //cout<< "nbsymb " << nbsymb;
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[j][i].getSymbole()==symb && symb!=Empty)
            {
                nbsymb++;
            }            
        }
        //cout<< "nbsymb " << nbsymb;
        if(nbsymb==3)
            res=true;
        if (symb==X)
            winner=player1;
        else
            winner=player2;
    }
    return res;
}

bool Morpion::GrilleFull()
{
    bool res=true;
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        cout<<"      ";
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[i][j].getSymbole()==Empty)
            {
                res=false;
            }
            
        }
    }
    return res;
}
