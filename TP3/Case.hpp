#ifndef case_h
#define case_h

enum Symbole {
    Empty=0,
    X=1,
    O=2 
};

class Case{
    private:
        Symbole symbole;
    public:
        Case();
        Symbole getSymbole();
        void setSymbole(Symbole symb);
        bool isFree();
        
};
#endif