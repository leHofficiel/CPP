#include "Case.hpp"


Case::Case()
{
    this->symbole=Empty;
}
Symbole Case::getSymbole()
{
    return this->symbole;
}

void Case::setSymbole(Symbole symb)
{
    this->symbole=symb;
}

bool Case::isFree()
{
    return (getSymbole()==Empty);
}