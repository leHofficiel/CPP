#include <iostream>

int somme(int a, int b)
{
    return a+b;
}

void inverse(int* a , int* b)
{
    int x= *a;
    *a=*b;
    *b=x;
}

void  sommePointeur (int a, int b, int* res){
    *res= a+b;
}

void sommeReference (int a, int b, int& res)
{
    res=a+b;
}

int main() {
    int a= 23, b=12, c=4,d=0;
    int res= somme(a,b);
    inverse(&a,&b);
    std::cout << res << "\n" ;
    sommePointeur(a,b,&c);
    std::cout << c <<"\n" ;
    sommeReference(a,b,d);
    std::cout << d <<"\n" ;
}
    