#include <iostream>
#include <string>

void guessNumber()
{
    srand(time(0));
    int nbAleat=rand()%1001;
    int nbEssai=0;
    int guessNumber;
    bool isFind =false;

    while (!isFind)
    {
        std::cout<< "Essaie de deviner le nombre mystère " << "\n";
        std::cin >> guessNumber;
        
        if(guessNumber==nbAleat){
            isFind=true;
            nbEssai++;
            std::cout<< "Bravo ! tu as trouvé le nombre mystère " << nbAleat << " en " << nbEssai << " coups\n";
        }
        else if (guessNumber > nbAleat){
            std::cout<< "Plus petit ! " << "\n";
            nbEssai++;}
        else if (guessNumber < nbAleat){
            std::cout<< "Plus grand ! " << "\n";
            nbEssai++;}
    }
}

int main(){
    guessNumber();
}