#include <iostream>

void aleat(int tab[], int size)
{
    for (int i = 0; i < size; i++)
    {
        tab[i]=rand()%100;
    }
    
}

void inverse(int* a , int* b)
{
    int x= *a;
    *a=*b;
    *b=x;
}

void triCroissant (int tab[], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j= 0; j<size;j++)
        {
            if (tab[j]>tab[i])
            {
                inverse(&tab[j],&tab[i]);
            }
        }
    }   
}

void triDecroissant (int tab[], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j= 0; j<size;j++)
        {
            if (tab[j]<tab[i])
            {
                inverse(&tab[j],&tab[i]);
            }
        }
    }  
}

void print(int tab[], int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout<< tab[i] << " | ";
    }
    std::cout<<"\n";
}

int main()
{
    int size;
    std::cout<<"Taille du tableau ?\n";
    std::cin >> size;
    int tab[size];
    aleat(tab,size);
    print(tab,size);
    std::cout<<"Tri ?\n1: Croissant \n2: Décroissant\n";
    int modeTrie;
    std::cin >> modeTrie;
    switch (modeTrie)
    {
    case 1:
        triCroissant(tab,size);
        print(tab,size);
        triDecroissant(tab,size);
        break;

    case 2:
        triDecroissant(tab,size);
        print(tab,size);
        triCroissant(tab,size);
        break;
    default:
        break;
    }
    print(tab,size);
}