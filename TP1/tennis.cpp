#include <iostream>

int getPoints(int nbEchanges)
{
    int score=0;
    if (nbEchanges > 3){ score=40;}
    else{
        switch (nbEchanges)
        {
        case 1:
            score=15;
            break;
        case 2:
            score=30;
            break;
        case 3:
            score=40;
            break;
        }
    }
    return score;
}
void getMatchResult(int nbEchanges1,int nbEchanges2)
{    
    int score1=getPoints(nbEchanges1);
    int score2=getPoints(nbEchanges2); 
    std::cout<< "\nScore " << score1 << " - " << score2 << std::endl;

    if(nbEchanges1 > nbEchanges2 && (nbEchanges1-nbEchanges2)>=2 && nbEchanges1>3){
        std::cout << "Le joueur 1 à remporté le match\n";
    }
    else if(nbEchanges2 > nbEchanges1 && (nbEchanges2-nbEchanges1)>=2 && nbEchanges2>3){
        std::cout << "Le joueur 2 à remporté le match\n";
    }
    else if(nbEchanges1 > nbEchanges2 && nbEchanges1>3){
        std::cout << "Avantage au joueur 1\n";
    }
    else if(nbEchanges2 > nbEchanges1 && nbEchanges2>3){
        std::cout << "Avantage au joueur 2\n";
    }
    else if(nbEchanges2 == nbEchanges1){
        std::cout << "Egalité\n";
    }      
}   

int main(){

    int nbEchanges1, nbEchanges2;
    std::cout << "Combien d'échanges a remporté le joueur 1 ?\n";
    std::cin >> nbEchanges1;
    std::cout << "Combien d'échanges a remporté le joueur 2 ?\n";
    std::cin >> nbEchanges2;
    getMatchResult(nbEchanges1,nbEchanges2);
}