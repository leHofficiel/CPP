#include <iostream>
#include <string>

void guessNumberIA()
{
    int min=0, max=1001;
    srand(time(0));
    int nbAleat=rand()%max;
    int nbEssai=0;
    int propositon=nbAleat;
    bool isFind =false;

    while (!isFind)
    {
        int res;
        std::cout<< propositon << " ? \n1: Plus grand\n2: Plus petit\n3: Bravo !\n";
        std:: cin >> res;
        switch (res)
        {
        case 1:
            min=propositon;
            propositon=(propositon+max)/2;
            nbEssai++;
            break;
        case 2:
            max=propositon;
            propositon=(propositon+min)/2;
            nbEssai++;
            break;
        case 3:
            isFind=true;
            nbEssai++;
            std::cout<<"Youpi ! j'ai deviné le nombre en " << nbEssai <<" essaies\n";
            break;
        default:
            break;
        }
    }
}

int main(){
    guessNumberIA();
}